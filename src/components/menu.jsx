import React from "react";

const Menu = () => {
    return (<nav id="menu">
        <div className="inner">
            <h2>Menu</h2>
            <ul className="links">
                <li><a href="index.html">Home</a></li>
                <li><a href="generic.html">Generic</a></li>
                <li><a href="elements.html">Elements</a></li>
                <li><a href="#">Log In</a></li>
                <li><a href="#">Sign Up</a></li>
            </ul>
        </div>
    </nav>);
};

export default Menu