import React from "react";
import { Link } from "gatsby"

const Header = () => {
    return (<header id="header">
        <h1><Link to='/'>Dousojin</Link></h1>
        <nav>
            <Link to='/past'>Past trips</Link>
            <Link to='/planned'>Planned trips</Link>
        </nav>
    </header>);
};

export default Header