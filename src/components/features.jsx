import React from "react";

const Features = () => {
    return (
        <section id="one" className="features">
            <header className="major">
                <h2>Magna sed consequat</h2>
                <p>Mauris lectus odio, mattis nec velit eu, luctus dictum diam. Quis<br/>
                    tempus que ornare purus a bibendum ultricies.</p>
            </header>
            <div className="content">
                <section className="feature">
                    <span className="icon major fa-gem"></span>
                    <h3>Etiam sed feugiat</h3>
                    <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum,
                        at ullamcorper libero dolor auctor sit amet. Proin vulputate amet sem ut tempus. Donec
                        quis ante viverra, suscipit euismod habitant lorem ipsum dolor.</p>
                </section>
                <section className="feature">
                    <span className="icon major fa-save"></span>
                    <h3>Ipsum et bibendum</h3>
                    <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum,
                        at ullamcorper libero dolor auctor sit amet. Proin vulputate amet sem ut tempus. Donec
                        quis ante viverra, suscipit euismod habitant lorem ipsum dolor.</p>
                </section>
                <section className="feature">
                    <span className="icon major fa-newspaper"></span>
                    <h3>Sit lorem aliquam</h3>
                    <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum,
                        at ullamcorper libero dolor auctor sit amet. Proin vulputate amet sem ut tempus. Donec
                        quis ante viverra, suscipit euismod habitant lorem ipsum dolor.</p>
                </section>
            </div>
        </section>
    );
};