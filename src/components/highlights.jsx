import rome from "../assets/images/rome.jpg";
import {Link} from "gatsby";
import tuscany from "../assets/images/tuscany.jpg";
import japan from "../assets/images/japan.jpg";
import React from "react";

export function RomeHighlight() {
    return <section id="rome" className="spotlight">
        <div className="image"><img src={rome} alt=""/></div>
        <div className="content">
            <h2>Rome</h2>
            <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum, at
                ullamcorper libero dolor auctor sit amet. Proin vulputate amet sem ut tempus. Donec quis
                ante viverra, suscipa facilisis at, vestibulum id urna. Lorem ipsum dolor sit amet
                sollicitudin.</p>
            <Link to="/rome" className="button">Read more</Link>
        </div>
    </section>;
}

export function TuscanyHighlight() {
    return <section id="three" className="spotlight alt">
        <div className="image"><img src={tuscany} alt=""/></div>
        <div className="content">
            <h2>Tuscany</h2>
            <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum, at
                ullamcorper libero dolor auctor sit amet. Proin vulputate amet sem ut tempus. Donec quis
                ante viverra, suscipa facilisis at, vestibulum id urna. Lorem ipsum dolor sit amet
                sollicitudin.</p>
            <Link to="/tuscany" className="button">Read more</Link>
        </div>
    </section>;
}

export function JapanHighlight() {
    return <section id="four" className="spotlight">
        <div className="image"><img src={japan} alt=""/></div>
        <div className="content">
            <h2>Japan</h2>
            <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum, at
                ullamcorper libero dolor auctor sit amet. Proin vulputate amet sem ut tempus. Donec quis
                ante viverra, suscipa facilisis at, vestibulum id urna. Lorem ipsum dolor sit amet
                sollicitudin.</p>
            <Link to="/japan" className="button">Read more</Link>
        </div>
    </section>;
}
