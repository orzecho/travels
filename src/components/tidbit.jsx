import React from "react";

export default function Tidbit({title, children}) {
    return <p className="tidbit">
        <h4><span className="icon solid fa-info-circle"/>{title}</h4>
        {children}
    </p>
}