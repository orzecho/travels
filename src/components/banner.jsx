import React from "react";

export default function Banner({header, actions, bannerClass}) {
    return (<section id="banner" className={bannerClass}>
        <header>
            {header}
        </header>
        <footer>
            <ul className="actions special">
                {actions}
            </ul>
        </footer>
    </section>);
}