import React from "react";
import {Link} from "gatsby";

export function Footer() {
    return <section id="footer">
        <div className="inner">
            <section className="about">
                <h3>Proin sed ultricies</h3>
                <p>Praesent egestas quam at lorem imperdiet lobortis. Mauris condimentum et euismod ipsum, at
                    ullamcorper libero dolor auctor sit amet. Proin vulputate.</p>
                <ul className="actions">
                    <li><Link to="/tuscany" className="button">Past trips</Link></li>
                    <li><Link to="/tuscany" className="button">Planned trips</Link></li>
                </ul>
            </section>
            <section>
                <h3>Get in Touch</h3>
                <ul className="contact">
                    <li className="icon solid fa-phone">(000) 000-0000</li>
                    <li className="icon solid fa-envelope"><a href="#">information@untitled.tld</a></li>
                    <li className="icon brands fa-twitter"><a href="#">@untitled-tld</a></li>
                    <li className="icon brands fa-facebook-f"><a href="#">facebook.com/untitled</a></li>
                </ul>
            </section>
            <section>
                <h3>Mailing Address</h3>
                <p>Untitled Corp<br/>
                    1234 Fictional Road<br/>
                    Suite 5432<br/>
                    Nashville, TN 00000<br/>
                    USA</p>
            </section>
        </div>
    </section>;
}