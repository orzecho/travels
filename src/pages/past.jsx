import React from 'react';
import '../assets/scss/main.scss';
import Header from "../components/header";
import Menu from "../components/menu";
import Banner from "../components/banner";
import Pulse from "react-reveal/Pulse";
import Fade from "react-reveal/Fade";
import Scroll from "react-scroll-to-element";

import rome_castle from '../assets/images/rome_castle.webp';
import {Footer} from "../components/footer";
import {JapanHighlight, RomeHighlight, TuscanyHighlight} from "../components/highlights";

export default function Past() {
    return (
        <div id="page-wrapper">

            <Header />

            <Menu />

            <div id="main">

                <RomeHighlight/>

                <TuscanyHighlight/>

                <JapanHighlight/>

            </div>

            <Footer />

        </div>
    );
}
