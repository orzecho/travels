import React from 'react';
import '../assets/scss/main.scss';
import Pulse from 'react-reveal/Pulse';
import Fade from 'react-reveal/Fade';
import Scroll from 'react-scroll-to-element';
import Header from '../components/header'
import Menu from '../components/menu'
import Banner from "../components/banner";
import {JapanHighlight, RomeHighlight, TuscanyHighlight} from "../components/highlights";
import {Footer} from "../components/footer";


export default function Index() {
    const header = <><Pulse><h2>Ipsum aliquam</h2></Pulse>
        <Fade><p>Sed feugiat tempus sem amet imperdiet</p></Fade></>;
    const actions = <li><Scroll type='id' element='rome' offset={-50}><a href="#" className="button large">Get
        Started</a></Scroll></li>;

    return (
        <div id="page-wrapper">

            <Header/>

            <Menu/>

            <div id="main">

                <Banner header={header} actions={actions}/>

                <RomeHighlight/>

                <TuscanyHighlight/>

                <JapanHighlight/>

            </div>

            <Footer />

        </div>
    );
}